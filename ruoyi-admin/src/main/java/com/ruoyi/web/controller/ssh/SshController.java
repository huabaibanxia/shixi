package com.ruoyi.web.controller.ssh;


import com.ruoyi.web.controller.ssh.domain.Link;
import com.ruoyi.web.websocket.service.WebSocketService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;


import javax.annotation.Resource;
import java.io.IOException;


/**
 * @author L
 */
@RestController
@RequestMapping("/ssh")
@Slf4j
public class SshController {

    @Resource
    private WebSocketService webSocketService;

    @PostMapping("/linkNew")
    public String linkNew(@RequestBody Link link) throws IOException {
        System.out.println(link);
        return webSocketService.createSession(link);
    }
}