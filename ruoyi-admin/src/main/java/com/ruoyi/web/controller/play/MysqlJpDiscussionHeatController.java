package com.ruoyi.web.controller.play;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.play.domain.MysqlJpDiscussionHeat;
import com.ruoyi.play.service.IMysqlJpDiscussionHeatService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;

import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 *
 * @author ruoyi
 * @date 2024-07-12
 */
@RestController
@RequestMapping("/play/likunyang")
public class MysqlJpDiscussionHeatController extends BaseController {
    @Autowired
    private IMysqlJpDiscussionHeatService mysqlJpDiscussionlikunyangService;

    /**
     * 查询【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('play:likunyang:list')")
    @GetMapping("/list")
    public TableDataInfo list(MysqlJpDiscussionHeat mysqlJpDiscussionlikunyang) {
        startPage();
        List<MysqlJpDiscussionHeat> list = mysqlJpDiscussionlikunyangService.selectMysqlJpDiscussionHeatList(mysqlJpDiscussionlikunyang);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('play:likunyang:export')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MysqlJpDiscussionHeat mysqlJpDiscussionlikunyang) {
        List<MysqlJpDiscussionHeat> list = mysqlJpDiscussionlikunyangService.selectMysqlJpDiscussionHeatList(mysqlJpDiscussionlikunyang);
        ExcelUtil<MysqlJpDiscussionHeat> util = new ExcelUtil<MysqlJpDiscussionHeat>(MysqlJpDiscussionHeat.class);
        util.exportExcel(response, list, "【请填写功能名称】数据");
    }

}
