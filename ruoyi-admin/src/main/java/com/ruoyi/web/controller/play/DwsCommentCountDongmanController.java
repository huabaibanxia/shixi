package com.ruoyi.web.controller.play;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.play.domain.DwsCommentCountDongman;
import com.ruoyi.play.service.IDwsCommentCountDongmanService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 各动漫的的评论数Controller
 *
 * @author ruoyi
 * @date 2024-07-11
 */
@RestController
@RequestMapping("/play/dongman")
public class DwsCommentCountDongmanController extends BaseController {
    @Autowired
    private IDwsCommentCountDongmanService dwsCommentCountDongmanService;

    /**
     * 查询各动漫的的评论数列表
     */
    @PreAuthorize("@ss.hasPermi('play:dongman:list')")
    @GetMapping("/list")
    public TableDataInfo list(DwsCommentCountDongman dwsCommentCountDongman) {
        startPage();
        List<DwsCommentCountDongman> list = dwsCommentCountDongmanService.selectDwsCommentCountDongmanList(dwsCommentCountDongman);
        return getDataTable(list);
    }

    /**
     * 导出各动漫的的评论数列表
     */
    @PreAuthorize("@ss.hasPermi('play:dongman:export')")
    @Log(title = "各动漫的的评论数", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DwsCommentCountDongman dwsCommentCountDongman) {
        List<DwsCommentCountDongman> list = dwsCommentCountDongmanService.selectDwsCommentCountDongmanList(dwsCommentCountDongman);
        ExcelUtil<DwsCommentCountDongman> util = new ExcelUtil<DwsCommentCountDongman>(DwsCommentCountDongman.class);
        util.exportExcel(response, list, "各动漫的的评论数数据");
    }

    /**
     * 获取各动漫的的评论数详细信息
     */
    @PreAuthorize("@ss.hasPermi('play:dongman:query')")
    @GetMapping(value = "/{dongmanName}")
    public AjaxResult getInfo(@PathVariable("dongmanName") String dongmanName) {
        return success(dwsCommentCountDongmanService.selectDwsCommentCountDongmanByDongmanName(dongmanName));
    }

    /**
     * 新增各动漫的的评论数
     */
    @PreAuthorize("@ss.hasPermi('play:dongman:add')")
    @Log(title = "各动漫的的评论数", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DwsCommentCountDongman dwsCommentCountDongman) {
        return toAjax(dwsCommentCountDongmanService.insertDwsCommentCountDongman(dwsCommentCountDongman));
    }

    /**
     * 修改各动漫的的评论数
     */
    @PreAuthorize("@ss.hasPermi('play:dongman:edit')")
    @Log(title = "各动漫的的评论数", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DwsCommentCountDongman dwsCommentCountDongman) {
        return toAjax(dwsCommentCountDongmanService.updateDwsCommentCountDongman(dwsCommentCountDongman));
    }

    /**
     * 删除各动漫的的评论数
     */
    @PreAuthorize("@ss.hasPermi('play:dongman:remove')")
    @Log(title = "各动漫的的评论数", businessType = BusinessType.DELETE)
    @DeleteMapping("/{dongmanNames}")
    public AjaxResult remove(@PathVariable String[] dongmanNames) {
        return toAjax(dwsCommentCountDongmanService.deleteDwsCommentCountDongmanByDongmanNames(dongmanNames));
    }
}
