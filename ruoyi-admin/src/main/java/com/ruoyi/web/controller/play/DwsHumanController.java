package com.ruoyi.web.controller.play;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.play.domain.DwsBeautifulCount;
import com.ruoyi.play.service.DwsBeautifulCountService;
import com.ruoyi.play.service.impl.DwsBeautifulCountServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jhl
 */
@RestController
@RequestMapping("/play/jianghailin")
public class DwsHumanController extends BaseController {


    @Autowired
    private DwsBeautifulCountService dwsBeautifulCountService;

    @PreAuthorize("@ss.hasPermi('play:jianghailin:list')")
    @GetMapping("/list")
    public TableDataInfo getDwsBeautifulCountList() {
        return getDataTable(dwsBeautifulCountService.selectDwsBeautifulCountList());
    }
}