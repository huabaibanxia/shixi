package com.ruoyi.web.controller.ssh.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * @author L
 */
@Data
public class Link implements Serializable {
    private String host;
    private String username;
    private String password;
    private Integer port = 22;
    private Long shijiancuo;

    private static final long serialVersionUID = 1L;
}
