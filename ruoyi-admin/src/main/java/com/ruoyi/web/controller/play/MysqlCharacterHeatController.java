package com.ruoyi.web.controller.play;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.play.domain.MysqlCharacterHeat;
import com.ruoyi.play.service.IMysqlCharacterHeatService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;

import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2024-07-12
 */
@RestController
@RequestMapping("/play/likunyang2")
public class MysqlCharacterHeatController extends BaseController
{
    @Autowired
    private IMysqlCharacterHeatService mysqlCharacterHeatService;

    /**
     * 查询【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('play:likunyang2:list')")
    @GetMapping("/list")
    public TableDataInfo list(MysqlCharacterHeat mysqlCharacterHeat)
    {
        startPage();
        List<MysqlCharacterHeat> list = mysqlCharacterHeatService.selectMysqlCharacterHeatList(mysqlCharacterHeat);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('play:likunyang2:export')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MysqlCharacterHeat mysqlCharacterHeat)
    {
        List<MysqlCharacterHeat> list = mysqlCharacterHeatService.selectMysqlCharacterHeatList(mysqlCharacterHeat);
        ExcelUtil<MysqlCharacterHeat> util = new ExcelUtil<MysqlCharacterHeat>(MysqlCharacterHeat.class);
        util.exportExcel(response, list, "【请填写功能名称】数据");
    }

    /**
     * 获取【请填写功能名称】详细信息
     */
    @PreAuthorize("@ss.hasPermi('play:likunyang2:query')")
    @GetMapping(value = "/{role}")
    public AjaxResult getInfo(@PathVariable("role") String role)
    {
        return success(mysqlCharacterHeatService.selectMysqlCharacterHeatByRole(role));
    }

    /**
     * 新增【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('play:likunyang2:add')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MysqlCharacterHeat mysqlCharacterHeat)
    {
        return toAjax(mysqlCharacterHeatService.insertMysqlCharacterHeat(mysqlCharacterHeat));
    }

    /**
     * 修改【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('play:likunyang2:edit')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MysqlCharacterHeat mysqlCharacterHeat)
    {
        return toAjax(mysqlCharacterHeatService.updateMysqlCharacterHeat(mysqlCharacterHeat));
    }

    /**
     * 删除【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('play:likunyang2:remove')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
	@DeleteMapping("/{roles}")
    public AjaxResult remove(@PathVariable String[] roles)
    {
        return toAjax(mysqlCharacterHeatService.deleteMysqlCharacterHeatByRoles(roles));
    }
}
