package com.ruoyi.web.controller.play;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.play.domain.DwsNameIpCountDongman;
import com.ruoyi.play.service.IDwsNameIpCountDongmanService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 对应动漫下的评论的ip地区对应的数量Controller
 *
 * @author ruoyi
 * @date 2024-07-12
 */
@RestController
@RequestMapping("/play/dongman/ip")
public class DwsNameIpCountDongmanController extends BaseController {
    @Autowired
    private IDwsNameIpCountDongmanService dwsNameIpCountDongmanService;

    /**
     * 查询对应动漫下的评论的ip地区对应的数量列表
     */
    @PreAuthorize("@ss.hasPermi('play:dongman:list')")
    @GetMapping("/list")
    public TableDataInfo list(DwsNameIpCountDongman dwsNameIpCountDongman) {
        startPage();
        List<DwsNameIpCountDongman> list = dwsNameIpCountDongmanService.selectDwsNameIpCountDongmanList(dwsNameIpCountDongman);
        return getDataTable(list);
    }

    /**
     * 导出对应动漫下的评论的ip地区对应的数量列表
     */
    @PreAuthorize("@ss.hasPermi('play:dongman:export')")
    @Log(title = "对应动漫下的评论的ip地区对应的数量", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DwsNameIpCountDongman dwsNameIpCountDongman) {
        List<DwsNameIpCountDongman> list = dwsNameIpCountDongmanService.selectDwsNameIpCountDongmanList(dwsNameIpCountDongman);
        ExcelUtil<DwsNameIpCountDongman> util = new ExcelUtil<DwsNameIpCountDongman>(DwsNameIpCountDongman.class);
        util.exportExcel(response, list, "对应动漫下的评论的ip地区对应的数量数据");
    }

    /**
     * 获取对应动漫下的评论的ip地区对应的数量详细信息
     */
    @PreAuthorize("@ss.hasPermi('play:dongman:query')")
    @GetMapping(value = "/{dongmanName}")
    public AjaxResult getInfo(@PathVariable("dongmanName") String dongmanName) {
        return success(dwsNameIpCountDongmanService.selectDwsNameIpCountDongmanByDongmanName(dongmanName));
    }

    /**
     * 新增对应动漫下的评论的ip地区对应的数量
     */
    @PreAuthorize("@ss.hasPermi('play:dongman:add')")
    @Log(title = "对应动漫下的评论的ip地区对应的数量", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DwsNameIpCountDongman dwsNameIpCountDongman) {
        return toAjax(dwsNameIpCountDongmanService.insertDwsNameIpCountDongman(dwsNameIpCountDongman));
    }

    /**
     * 修改对应动漫下的评论的ip地区对应的数量
     */
    @PreAuthorize("@ss.hasPermi('system:dongman:edit')")
    @Log(title = "对应动漫下的评论的ip地区对应的数量", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DwsNameIpCountDongman dwsNameIpCountDongman) {
        return toAjax(dwsNameIpCountDongmanService.updateDwsNameIpCountDongman(dwsNameIpCountDongman));
    }

    /**
     * 删除对应动漫下的评论的ip地区对应的数量
     */
    @PreAuthorize("@ss.hasPermi('system:dongman:remove')")
    @Log(title = "对应动漫下的评论的ip地区对应的数量", businessType = BusinessType.DELETE)
    @DeleteMapping("/{dongmanNames}")
    public AjaxResult remove(@PathVariable String[] dongmanNames) {
        return toAjax(dwsNameIpCountDongmanService.deleteDwsNameIpCountDongmanByDongmanNames(dongmanNames));
    }
}
