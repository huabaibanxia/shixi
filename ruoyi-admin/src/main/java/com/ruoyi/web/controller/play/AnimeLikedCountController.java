package com.ruoyi.web.controller.play;

import java.util.List;


import com.ruoyi.play.domain.AnimeLikedCount;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ruoyi.common.core.controller.BaseController;


import com.ruoyi.system.service.IAnimeLikedCountService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 *
 * @author ruoyi
 * @date 2024-07-12
 */
@RestController
@RequestMapping("/play/xiongqi")
public class AnimeLikedCountController extends BaseController {
    @Autowired
    private IAnimeLikedCountService animeLikedCountService;

    /**
     * 查询【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('play:xiongqi:list')")
    @GetMapping("/list")
    public TableDataInfo list(AnimeLikedCount animeLikedCount) {
        startPage();
        List<AnimeLikedCount> list = animeLikedCountService.selectAnimeLikedCountList(animeLikedCount);
        return getDataTable(list);
    }
}
