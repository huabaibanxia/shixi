package com.ruoyi.web.controller.play;

import java.util.List;


import com.ruoyi.play.domain.AnimeCommentTop10;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ruoyi.common.core.controller.BaseController;

import com.ruoyi.system.service.IAnimeCommentTop10Service;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 *
 * @author ruoyi
 * @date 2024-07-12
 */
@RestController
@RequestMapping("/play/xiongqi2")
public class AnimeCommentTop10Controller extends BaseController {
    @Autowired
    private IAnimeCommentTop10Service animeCommentTop10Service;

    /**
     * 查询【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('play:xiongqi2:list')")
    @GetMapping("/list")
    public TableDataInfo list(AnimeCommentTop10 animeCommentTop10) {
        startPage();
        List<AnimeCommentTop10> list = animeCommentTop10Service.selectAnimeCommentTop10List(animeCommentTop10);
        return getDataTable(list);
    }
}
