package com.ruoyi.web.controller.play;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.play.domain.DwsIpCountDongman;
import com.ruoyi.play.service.IDwsIpCountDongmanService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 所有的ip地区对应的数量Controller
 *
 * @author ruoyi
 * @date 2024-07-12
 */
@RestController
@RequestMapping("/play/dongman2")
public class DwsIpCountDongmanController extends BaseController {
    @Autowired
    private IDwsIpCountDongmanService dwsIpCountDongmanService;

    /**
     * 查询所有的ip地区对应的数量列表
     */
    @PreAuthorize("@ss.hasPermi('play:dongman2:list')")
    @GetMapping("/list")
    public TableDataInfo list(DwsIpCountDongman dwsIpCountDongman) {
        startPage();
        List<DwsIpCountDongman> list = dwsIpCountDongmanService.selectDwsIpCountDongmanList(dwsIpCountDongman);
        return getDataTable(list);
    }

    /**
     * 导出所有的ip地区对应的数量列表
     */
    @PreAuthorize("@ss.hasPermi('play:dongman2:export')")
    @Log(title = "所有的ip地区对应的数量", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DwsIpCountDongman dwsIpCountDongman) {
        List<DwsIpCountDongman> list = dwsIpCountDongmanService.selectDwsIpCountDongmanList(dwsIpCountDongman);
        ExcelUtil<DwsIpCountDongman> util = new ExcelUtil<DwsIpCountDongman>(DwsIpCountDongman.class);
        util.exportExcel(response, list, "所有的ip地区对应的数量数据");
    }

    /**
     * 获取所有的ip地区对应的数量详细信息
     */
    @PreAuthorize("@ss.hasPermi('play:dongman2:query')")
    @GetMapping(value = "/{ipLocation}")
    public AjaxResult getInfo(@PathVariable("ipLocation") String ipLocation) {
        return success(dwsIpCountDongmanService.selectDwsIpCountDongmanByIpLocation(ipLocation));
    }

    /**
     * 新增所有的ip地区对应的数量
     */
    @PreAuthorize("@ss.hasPermi('play:dongman2:add')")
    @Log(title = "所有的ip地区对应的数量", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DwsIpCountDongman dwsIpCountDongman) {
        return toAjax(dwsIpCountDongmanService.insertDwsIpCountDongman(dwsIpCountDongman));
    }

    /**
     * 修改所有的ip地区对应的数量
     */
    @PreAuthorize("@ss.hasPermi('play:dongman2:edit')")
    @Log(title = "所有的ip地区对应的数量", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DwsIpCountDongman dwsIpCountDongman) {
        return toAjax(dwsIpCountDongmanService.updateDwsIpCountDongman(dwsIpCountDongman));
    }

    /**
     * 删除所有的ip地区对应的数量
     */
    @PreAuthorize("@ss.hasPermi('play:dongman2:remove')")
    @Log(title = "所有的ip地区对应的数量", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ipLocations}")
    public AjaxResult remove(@PathVariable String[] ipLocations) {
        return toAjax(dwsIpCountDongmanService.deleteDwsIpCountDongmanByIpLocations(ipLocations));
    }
}
