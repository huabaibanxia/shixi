package com.ruoyi.web.websocket.domain.vo.resp;

import lombok.Data;

/**
 * @author L
 */
@Data
public class WSBaseResp {



    private String key;

    private WSDataResp data;

    public WSBaseResp(String channelKey, WSDataResp data) {
        this.key = channelKey;
        this.data = data;
    }
}
