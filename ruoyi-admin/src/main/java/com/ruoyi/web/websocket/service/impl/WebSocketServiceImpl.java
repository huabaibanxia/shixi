package com.ruoyi.web.websocket.service.impl;

import cn.hutool.json.JSONUtil;

import com.ruoyi.web.controller.ssh.domain.Link;
import com.ruoyi.web.websocket.domain.vo.req.WSBaseRequest;
import com.ruoyi.web.websocket.domain.vo.resp.WSBaseResp;
import com.ruoyi.web.websocket.domain.vo.resp.WSDataResp;
import com.ruoyi.web.websocket.service.WebSocketService;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.sshd.client.SshClient;
import org.apache.sshd.client.channel.ChannelShell;
import org.apache.sshd.client.session.ClientSession;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * description: WebSocketServiceImpl: 管理WebSocket逻辑
 *
 * @author L
 */
@Service
@Slf4j
public class WebSocketServiceImpl implements WebSocketService {

    private static final ConcurrentHashMap<String, SshClient> CLIENT_SSH = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<String, ClientSession> SESSIONS_CLIENT = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<String, ChannelShell> CHANNELS_CLIENT = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<String, PipedOutputStream> OUTPUT_CLIENT = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<String, List<String>> SESSION_CHANNEL_LIST = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<String, String> CHANNEL_SESSION = new ConcurrentHashMap<>();
    private static final Map<String, Thread> CHANNEL_THREADS = new ConcurrentHashMap<>();
    public static final String SESSION = "session_";
    public static final String CHANNEL = "channel_";
    /**
     * wschannel映射    ssh channel 列表
     */
    private static final ConcurrentHashMap<Channel, List<String>> ONLINE_USER_CHANNEL_MAP = new ConcurrentHashMap<>();
    /**
     * 创建管道最大等待时间 s
     */
    public static final int TIMEOUT_MILLIS = 9;

    private String getSessionKey(Link link) {
        return SESSION + link.getHost() + link.getUsername() + "_" + link.getShijiancuo();
    }

    @Override
    public String createSession(Link link) {
        String key = getSessionKey(link);
        // 如果会话已经存在，则直接返回会话ID
        if (CLIENT_SSH.containsKey(key)) {
            return key;
        }
        String host = link.getHost();
        String username = link.getUsername();
        String password = link.getPassword();
        Integer port = link.getPort();
        SshClient client = SshClient.setUpDefaultClient();
        try {
            client.start();
            ClientSession session = client.connect(username, host, port).verify().getSession();
            session.addPasswordIdentity(password); // 假设使用密码认证
            if (session.auth().verify().isSuccess()) {
                CLIENT_SSH.put(key, client);
                SESSIONS_CLIENT.put(key, session);
                return key; // 返回会话ID
            }
        } catch (Exception e) {
            log.error("{e}", e);
            return "连接失败！";
        }
        return null;
    }

    private String getChannelKey(String sessionId, Integer index) {
        return CHANNEL + sessionId + "_" + index;
    }

    public ClientSession getSession(String sessionId) {
        return SESSIONS_CLIENT.get(sessionId);
    }

    private void createTh(String channelKey, Channel wsChannel) {
        if (CHANNEL_THREADS.containsKey(channelKey)) {
            return;
        }
        // 启动一个新的线程来读取SSH的输出流  绑定对应的wsChannel 响应
        Thread thread = new Thread(() -> {
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(CHANNELS_CLIENT.get(channelKey).getInvertedOut()));
                String line;
                while ((line = reader.readLine()) != null) {
                    this.sendMsg(wsChannel, new WSBaseResp(channelKey, new WSDataResp(line, 0)));
                }
            } catch (IOException e) {
                log.error("创建对应管道" + channelKey + "线程出错", e);
            }

        });
        thread.start();
        CHANNEL_THREADS.put(channelKey, thread);
    }

    @Override
    public void jianChannel(Channel wsChannel, String sessionId, Integer index) throws IOException {
        ClientSession session = this.getSession(sessionId);
        if (session != null) {
            String channelKey = getChannelKey(sessionId, index);
            // 如果已经存在 表示用户想建立已有的管道 直接返回
            if (CHANNELS_CLIENT.containsKey(channelKey)) {
                this.sendMsg(wsChannel, new WSBaseResp(channelKey, new WSDataResp(channelKey, 1)));
            }
            ChannelShell channel = session.createShellChannel();
            PipedInputStream in = new PipedInputStream();
            channel.setIn(in);
            PipedOutputStream out = new PipedOutputStream(in);

//            channel.setOut(System.out); // 设置输出到控制台
            channel.setErr(System.err);
            channel.open().verify();
            CHANNELS_CLIENT.put(channelKey, channel);
            OUTPUT_CLIENT.put(channelKey, out);
            CHANNEL_SESSION.put(channelKey, sessionId);
            SESSION_CHANNEL_LIST.computeIfAbsent(sessionId, k -> new CopyOnWriteArrayList<>()).add(channelKey);
            createTh(channelKey, wsChannel);
            ONLINE_USER_CHANNEL_MAP.computeIfAbsent(wsChannel, k -> new CopyOnWriteArrayList<>()).add(channelKey);
            this.sendMsg(wsChannel, new WSBaseResp(channelKey, new WSDataResp(channelKey, 1)));
        }
    }

    @Override
    public void connect(Channel channel) {
        ONLINE_USER_CHANNEL_MAP.put(channel, new ArrayList<>());
    }

    // 当用户关掉对应标签页的channelId 时

    // 当用户关闭浏览器时 或心跳停止
    @Override
    public void removed(Channel channel) {

        List<String> strings = ONLINE_USER_CHANNEL_MAP.get(channel);
        if (strings != null) {
            Set<String> sessionIds = new HashSet<>();
            for (String channelKey : strings) {
                String se = CHANNEL_SESSION.get(channelKey);
                if (StringUtils.isNotBlank(se)) {
                    CHANNEL_SESSION.remove(channelKey);
                    // 关闭对应的 channel
                    closeChannel(channelKey);
                    // 关闭channel 的 线程
                    closeThread(channelKey);
                    // 关闭channel 对应 out
                    closeChannelOut(channelKey);
                    sessionIds.add(se);
                }
            }
            if (!sessionIds.isEmpty()) {
                for (String sessionId : sessionIds) {
                    List<String> channelKeys = SESSION_CHANNEL_LIST.get(sessionId);
                    if (channelKeys != null) {
                        SESSION_CHANNEL_LIST.remove(sessionId);
                    }
                    // 关闭session
                    closeSession(sessionId);
                    // 关闭client
                    closeClient(sessionId);
                }
            }
        }
        ONLINE_USER_CHANNEL_MAP.remove(channel);
    }

    private void closeChannelOut(String channelKey) {
        if (OUTPUT_CLIENT.containsKey(channelKey)) {
            try {
                OUTPUT_CLIENT.get(channelKey).close();
            } catch (IOException e) {
                log.error("关闭管道" + channelKey + "出错", e);
            }
            OUTPUT_CLIENT.remove(channelKey);
        }
    }

    private void closeChannel(String channelKey) {
        if (CHANNELS_CLIENT.containsKey(channelKey)) {
            try {
                CHANNELS_CLIENT.get(channelKey).close();
            } catch (IOException e) {
                log.error("关闭管道" + channelKey + "出错", e);
            }
            CHANNELS_CLIENT.remove(channelKey);
        }
    }

    private void closeThread(String channelKey) {
        if (CHANNEL_THREADS.containsKey(channelKey)) {
            CHANNEL_THREADS.get(channelKey).interrupt();
            CHANNEL_THREADS.remove(channelKey);
        }
    }

    private void closeSession(String sessionId) {
        if (SESSIONS_CLIENT.containsKey(sessionId)) {
            try {
                SESSIONS_CLIENT.get(sessionId).close();
            } catch (IOException e) {
                log.error("关闭会话" + sessionId + "出错", e);
            }
            SESSIONS_CLIENT.remove(sessionId);
        }
    }

    private void closeClient(String sessionId) {
        if (CLIENT_SSH.containsKey(sessionId)) {
            try {
                CLIENT_SSH.get(sessionId).close();
            } catch (IOException e) {
                log.error("关闭client" + sessionId + "出错", e);
            }
            CLIENT_SSH.remove(sessionId);
        }
    }

    @Override
    public void command(Channel channel, WSBaseRequest wsBaseRequest) throws IOException {
        String channelId = wsBaseRequest.getChannelId();
        String command = wsBaseRequest.getData();
        ChannelShell channelShell = CHANNELS_CLIENT.get(channelId);

        if (channelShell != null) {

            PipedOutputStream out = OUTPUT_CLIENT.get(channelId);
            if (out != null) {
                if ("ctrl+c".equals(command) || "ctrl+c\n".equals(command)) {
//                channelShell.getExitSignal();
                    out.write(3);
                    out.write("\n".getBytes());
                    out.write("\n".getBytes());
                    out.flush();
                } else {
                    out.write((command + "\n").getBytes());
                    out.flush();
                }
            }
        }
    }

    @Override
    public void remove(Channel channel, String channelId) {
        // 从ONLINE_USER_CHANNEL_MAP移除对应的channelId
        ONLINE_USER_CHANNEL_MAP.get(channel).remove(channelId);
        closeChannel(channelId);
        closeThread(channelId);
        closeChannelOut(channelId);
        String se = CHANNEL_SESSION.get(channelId);
        if (se != null) {
            CHANNEL_SESSION.remove(channelId);
            List<String> list = SESSION_CHANNEL_LIST.get(se);
            if (list != null) {
                list.remove(channelId);
            }
            // 判断list 是否为空 为空的话
            if (list == null || list.isEmpty()) {
                SESSION_CHANNEL_LIST.remove(se);
                // 判断存在后移除

                ClientSession clientSession = SESSIONS_CLIENT.get(se);
                if (clientSession != null) {
                    try {
                        clientSession.close();
                        SESSIONS_CLIENT.remove(se);
                    } catch (IOException e) {
                        log.error("移除会话" + se + "出错", e);
                    }
                }
                SshClient sshClient = CLIENT_SSH.get(se);
                if (sshClient != null) {
                    sshClient.stop();
                    CLIENT_SSH.remove(se);
                }
            }
        }
    }


    private void sendMsg(Channel channel, WSBaseResp wsBaseRequest) {
        channel.writeAndFlush(new TextWebSocketFrame(JSONUtil.toJsonStr(wsBaseRequest)));
    }


}
