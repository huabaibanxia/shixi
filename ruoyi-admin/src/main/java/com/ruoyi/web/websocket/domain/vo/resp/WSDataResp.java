package com.ruoyi.web.websocket.domain.vo.resp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author L
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WSDataResp {
    private String data;
    /**
     * 0:ssh 响应的命令数据行 1:表示建立channel返回的channelId
     */
    private Integer type;
}
