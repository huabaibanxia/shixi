package com.ruoyi.web.websocket.domain.dto;

import lombok.Data;

/**
 * @author L
 */
@Data
public class WSChannelExtraDTO {
    private Long uid;
}
