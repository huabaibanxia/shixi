package com.ruoyi.web.websocket.service;


import com.ruoyi.web.controller.ssh.domain.Link;
import com.ruoyi.web.websocket.domain.vo.req.WSBaseRequest;
import io.netty.channel.Channel;

import java.io.IOException;


/**
 * @author L
 */
public interface WebSocketService {
    String createSession(Link link);

    void jianChannel(Channel channel,String sessionId, Integer index) throws IOException;

    void connect(Channel channel);


    void removed(Channel channel);


    void command(Channel channel, WSBaseRequest wsBaseRequest) throws IOException;

    void remove(Channel channel, String channelId);
}
