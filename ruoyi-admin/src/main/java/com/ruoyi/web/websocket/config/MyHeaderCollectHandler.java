package com.ruoyi.web.websocket.config;

import cn.hutool.core.net.url.UrlBuilder;

import com.ruoyi.web.websocket.utils.NettyUtils;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.HttpRequest;
import org.apache.commons.lang3.StringUtils;

import java.net.InetSocketAddress;
import java.util.Optional;

/**
 * @author L
 * @description:
 */
public class MyHeaderCollectHandler extends ChannelInboundHandlerAdapter {


    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (msg instanceof HttpRequest) {
            final HttpRequest req = (HttpRequest) msg;
            UrlBuilder urlBuilder = UrlBuilder.ofHttp(req.uri());
            Optional<String> token = Optional.of(urlBuilder)
                    .map(UrlBuilder::getQuery)
                    .map(k -> k.get("token"))
                    .map(CharSequence::toString);
            token.ifPresent(s -> NettyUtils.setAttr(ctx.channel(), NettyUtils.TOKEN, s));
            req.setUri(urlBuilder.getPath().toString());
            // 取用户ip
            String ip = req.headers().get("X-Real-IP");
            if (StringUtils.isBlank(ip)) {
                InetSocketAddress address = (InetSocketAddress) ctx.channel().remoteAddress();
                ip = address.getAddress().getHostAddress();
            }
            NettyUtils.setAttr(ctx.channel(), NettyUtils.IP, ip);
            // 处理器只需要处理一次
            ctx.pipeline().remove(this);
        }
        ctx.fireChannelRead(msg);

    }


}
