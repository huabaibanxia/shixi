package com.ruoyi.web.websocket.domain.vo.req;

import lombok.Data;

/**
 * @author L
 */
@Data
public class WSBaseRequest {
    /**
     * @see com.example.sx.websocket.domain.enums.WSReqTypeEnum
     */
    private Integer type;
    private String channelId;
    private String data;
    private String sessionId;
    private Integer index;
}
