package com.ruoyi.web.websocket.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * Description: ws前端请求类型枚举
 * Date: 2023-03-19
 *
 * @author L
 */
@AllArgsConstructor
@Getter
public enum WSReqTypeEnum {
    CHANNEL(1, "前端发送命令"),
    JIAN(0, "建立sshChannel"),
    HEARTBEAT(2, "心跳包"),
    REMOVE_CHANNEL_ID(3, "当用户关闭某个标签页发送关闭的channelID"),
    ;

    private final Integer type;
    private final String desc;

    public static WSReqTypeEnum of(Integer type) {
        return Arrays.stream(WSReqTypeEnum.values())
                .filter(e -> e.getType().equals(type))
                .findFirst()
                .orElse(null);
    }
}
