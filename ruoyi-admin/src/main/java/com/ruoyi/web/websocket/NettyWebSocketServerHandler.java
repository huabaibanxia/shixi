package com.ruoyi.web.websocket;


import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONUtil;

import com.ruoyi.web.websocket.domain.enums.WSReqTypeEnum;
import com.ruoyi.web.websocket.domain.vo.req.WSBaseRequest;
import com.ruoyi.web.websocket.service.WebSocketService;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import lombok.extern.slf4j.Slf4j;



/**
 * @author L
 */
@Slf4j
@Sharable
public class NettyWebSocketServerHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {

    private WebSocketService webSocketService;


    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        // 最开始的时候，将WebSocketService注入进来
        webSocketService = SpringUtil.getBean(WebSocketService.class);
        webSocketService.connect(ctx.channel());
        log.info("连接成功");
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        userOffLine(ctx);
    }

    /**
     * 当用户事件被触发时调用此方法。主要处理WebSocket连接的手势完成和空闲状态事件。
     *
     * @param ctx ChannelHandlerContext，用于执行I/O操作的上下文。
     * @param evt 触发的事件对象，可以是握手完成事件或空闲状态事件。
     * @throws Exception 如果处理事件时发生异常。
     */
    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        // 检查是否为握手完成事件
        if (evt instanceof WebSocketServerProtocolHandler.HandshakeComplete) {
            log.info("握手完成！");
//            // 从通道中获取token，如果存在且不为空，则进行授权操作
//            String token = NettyUtils.getAttr(ctx.channel(), NettyUtils.TOKEN);
//            if (StringUtils.isNotBlank(token)) {
//                webSocketService.authorize(ctx.channel());
//            }
        } else if (evt instanceof IdleStateEvent) {
            IdleStateEvent event = (IdleStateEvent) evt;
            // 检查是否为读空闲超时事件
            if (event.state() == IdleState.READER_IDLE) {
                log.info("读空闲超时");
                // 关闭用户连接
                // 连接关闭
                userOffLine(ctx);
            }
        /*else if (evt == WebSocketServerProtocolHandler.ServerHandshakeStateEvent.HANDSHAKE_COMPLETE) {
            log.info("握手请求");
            String token = NettyUtils.getAttr(ctx.channel(), NettyUtils.TOKEN);
            if (StringUtils.isNotBlank(token)) {
                webSocketService.authorize(ctx.channel(), token);
            }
        }*/
        }
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        log.error("异常", cause);
        userOffLine(ctx);
    }

    /**
     * 用户下线
     *
     * @param ctx 上下文
     */
    private void userOffLine(ChannelHandlerContext ctx) {
        webSocketService.removed(ctx.channel());
        ctx.channel().close();
    }


    /**
     * 读取客户端发送的请求报文
     *
     * @param ctx
     * @param msg
     * @throws Exception
     */
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, TextWebSocketFrame msg) throws Exception {
        String text = msg.text();
        if (text == null) {
            return;
        }
        // 如果不是json格式的数据，直接返回
        if (!JSONUtil.isTypeJSON(text)) {
            return;
        }

        WSBaseRequest wsBaseRequest = JSONUtil.toBean(text, WSBaseRequest.class);
        Integer type = wsBaseRequest.getType();
        WSReqTypeEnum wsReqTypeEnum = WSReqTypeEnum.of(type);
        switch (wsReqTypeEnum) {
            case HEARTBEAT:
                break;
            case CHANNEL:
                webSocketService.command(ctx.channel(), wsBaseRequest);
                break;
            case JIAN:
                webSocketService.jianChannel(ctx.channel(), wsBaseRequest.getSessionId(), wsBaseRequest.getIndex());
                break;
            case REMOVE_CHANNEL_ID:
                webSocketService.remove(ctx.channel(), wsBaseRequest.getChannelId());
                break;
        }
        System.out.println(text);
    }
}
