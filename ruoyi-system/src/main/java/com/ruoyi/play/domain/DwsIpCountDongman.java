package com.ruoyi.play.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 所有的ip地区对应的数量对象 dws_ip_count_dongman
 *
 * @author ruoyi
 * @date 2024-07-12
 */
public class DwsIpCountDongman extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * IP地区
     */
    @Excel(name = "IP地区")
    private String ipLocation;

    /**
     * 该ip地区的数量
     */
    @Excel(name = "该ip地区的数量")
    private Long ipCount;

    private Integer sc;

    public Integer getSc() {
        return sc;
    }

    public void setSc(Integer sc) {
        this.sc = sc;
    }

    public void setIpLocation(String ipLocation) {
        this.ipLocation = ipLocation;
    }

    public String getIpLocation() {
        return ipLocation;
    }

    public void setIpCount(Long ipCount) {
        this.ipCount = ipCount;
    }

    public Long getIpCount() {
        return ipCount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("ipLocation", getIpLocation())
                .append("ipCount", getIpCount())
                .toString();
    }
}
