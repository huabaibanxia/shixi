package com.ruoyi.play.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 各动漫的的评论数对象 dws_comment_count_dongman
 *
 * @author ruoyi
 * @date 2024-07-11
 */
public class DwsCommentCountDongman extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 动漫名
     */
    @Excel(name = "动漫名")
    private String dongmanName;

    /**
     * 评论数
     */
    @Excel(name = "评论数")
    private Long commentCount;

    /**
     * 0 升序 1 降序
     */
    private Integer sc;

    public Integer getSc() {
        return sc;
    }

    public void setSc(Integer sc) {
        this.sc = sc;
    }

    public void setDongmanName(String dongmanName) {
        this.dongmanName = dongmanName;
    }

    public String getDongmanName() {
        return dongmanName;
    }

    public void setCommentCount(Long commentCount) {
        this.commentCount = commentCount;
    }

    public Long getCommentCount() {
        return commentCount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("dongmanName", getDongmanName())
                .append("commentCount", getCommentCount())
                .toString();
    }
}
