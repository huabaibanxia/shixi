package com.ruoyi.play.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 mysql_jp_discussion_heat
 * 
 * @author ruoyi
 * @date 2024-07-12
 */
public class MysqlJpDiscussionHeat extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String yearMonth;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long commentCount;

    public void setYearMonth(String yearMonth) 
    {
        this.yearMonth = yearMonth;
    }

    public String getYearMonth() 
    {
        return yearMonth;
    }
    public void setCommentCount(Long commentCount) 
    {
        this.commentCount = commentCount;
    }

    public Long getCommentCount() 
    {
        return commentCount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("yearMonth", getYearMonth())
            .append("commentCount", getCommentCount())
            .toString();
    }
}
