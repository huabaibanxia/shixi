package com.ruoyi.play.domain;

import java.io.Serializable;

/**
 * @TableName dws_beautiful_count
 */
public class DwsBeautifulCount implements Serializable {


    /**
     *
     */
    private String ipLocation;


    /**
     *
     */
    private Integer beautifulCount;
    private Integer coolCount;

    public Integer getCoolCount() {
        return coolCount;
    }

    public void setCoolCount(Integer coolCount) {
        this.coolCount = coolCount;
    }

    public Integer getBeautifulCount() {
        return beautifulCount;
    }

    public void setBeautifulCount(Integer beautifulCount) {
        this.beautifulCount = beautifulCount;
    }

    public String getIpLocation() {
        return ipLocation;
    }

    public void setIpLocation(String ipLocation) {
        this.ipLocation = ipLocation;
    }


}