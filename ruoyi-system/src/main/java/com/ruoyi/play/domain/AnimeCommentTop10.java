package com.ruoyi.play.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 anime_comment_top10
 *
 * @author ruoyi
 * @date 2024-07-12
 */
public class AnimeCommentTop10 extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String animeName;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long totalCommentCount;

    public void setAnimeName(String animeName) {
        this.animeName = animeName;
    }

    public String getAnimeName() {
        return animeName;
    }

    public void setTotalCommentCount(Long totalCommentCount) {
        this.totalCommentCount = totalCommentCount;
    }

    public Long getTotalCommentCount() {
        return totalCommentCount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("animeName", getAnimeName())
                .append("totalCommentCount", getTotalCommentCount())
                .toString();
    }
}
