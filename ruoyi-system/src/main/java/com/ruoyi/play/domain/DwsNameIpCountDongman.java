package com.ruoyi.play.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 对应动漫下的评论的ip地区对应的数量对象 dws_name_ip_count_dongman
 *
 * @author ruoyi
 * @date 2024-07-12
 */
public class DwsNameIpCountDongman extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 动漫名
     */
    @Excel(name = "动漫名")
    private String dongmanName;

    /**
     * ip地区
     */
    @Excel(name = "ip地区")
    private String ipLocation;

    /**
     * 该动漫的该ip地区的数量
     */
    @Excel(name = "该动漫的该ip地区的数量")
    private Long commentCount;
    /**
     * 0- 升序 1-降序
     */
    private Integer sc;

    public Integer getSc() {
        return sc;
    }

    public void setSc(Integer sc) {
        this.sc = sc;
    }

    public void setDongmanName(String dongmanName) {
        this.dongmanName = dongmanName;
    }

    public String getDongmanName() {
        return dongmanName;
    }

    public void setIpLocation(String ipLocation) {
        this.ipLocation = ipLocation;
    }

    public String getIpLocation() {
        return ipLocation;
    }

    public void setCommentCount(Long commentCount) {
        this.commentCount = commentCount;
    }

    public Long getCommentCount() {
        return commentCount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("dongmanName", getDongmanName())
                .append("ipLocation", getIpLocation())
                .append("commentCount", getCommentCount())
                .toString();
    }
}
