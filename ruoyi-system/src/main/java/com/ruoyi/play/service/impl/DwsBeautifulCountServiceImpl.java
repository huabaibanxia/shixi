package com.ruoyi.play.service.impl;


import com.ruoyi.play.domain.DwsBeautifulCount;
import com.ruoyi.play.service.DwsBeautifulCountService;
import com.ruoyi.play.mapper.DwsBeautifulCountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
* @author Administrator
* @description 针对表【dws_beautiful_count】的数据库操作Service实现
* @createDate 2024-07-12 16:35:10
*/
@Service
public class DwsBeautifulCountServiceImpl implements DwsBeautifulCountService{
    @Autowired
    DwsBeautifulCountMapper dwsBeautifulCountMapper;
    @Override
    public List<DwsBeautifulCount> selectDwsBeautifulCountList() {
        return  dwsBeautifulCountMapper.selectDwsBeautifulCountList();
    }
}