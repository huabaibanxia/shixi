package com.ruoyi.play.service;

import com.ruoyi.play.domain.DwsNameIpCountDongman;

import java.util.List;


/**
 * 对应动漫下的评论的ip地区对应的数量Service接口
 * 
 * @author ruoyi
 * @date 2024-07-12
 */
public interface IDwsNameIpCountDongmanService 
{
    /**
     * 查询对应动漫下的评论的ip地区对应的数量
     * 
     * @param dongmanName 对应动漫下的评论的ip地区对应的数量主键
     * @return 对应动漫下的评论的ip地区对应的数量
     */
    public DwsNameIpCountDongman selectDwsNameIpCountDongmanByDongmanName(String dongmanName);

    /**
     * 查询对应动漫下的评论的ip地区对应的数量列表
     * 
     * @param dwsNameIpCountDongman 对应动漫下的评论的ip地区对应的数量
     * @return 对应动漫下的评论的ip地区对应的数量集合
     */
    public List<DwsNameIpCountDongman> selectDwsNameIpCountDongmanList(DwsNameIpCountDongman dwsNameIpCountDongman);

    /**
     * 新增对应动漫下的评论的ip地区对应的数量
     * 
     * @param dwsNameIpCountDongman 对应动漫下的评论的ip地区对应的数量
     * @return 结果
     */
    public int insertDwsNameIpCountDongman(DwsNameIpCountDongman dwsNameIpCountDongman);

    /**
     * 修改对应动漫下的评论的ip地区对应的数量
     * 
     * @param dwsNameIpCountDongman 对应动漫下的评论的ip地区对应的数量
     * @return 结果
     */
    public int updateDwsNameIpCountDongman(DwsNameIpCountDongman dwsNameIpCountDongman);

    /**
     * 批量删除对应动漫下的评论的ip地区对应的数量
     * 
     * @param dongmanNames 需要删除的对应动漫下的评论的ip地区对应的数量主键集合
     * @return 结果
     */
    public int deleteDwsNameIpCountDongmanByDongmanNames(String[] dongmanNames);

    /**
     * 删除对应动漫下的评论的ip地区对应的数量信息
     * 
     * @param dongmanName 对应动漫下的评论的ip地区对应的数量主键
     * @return 结果
     */
    public int deleteDwsNameIpCountDongmanByDongmanName(String dongmanName);
}
