package com.ruoyi.play.service.impl;

import java.util.List;

import com.ruoyi.play.domain.MysqlCharacterHeat;
import com.ruoyi.play.mapper.MysqlCharacterHeatMapper;
import com.ruoyi.play.service.IMysqlCharacterHeatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-07-12
 */
@Service
public class MysqlCharacterHeatServiceImpl implements IMysqlCharacterHeatService
{
    @Autowired
    private MysqlCharacterHeatMapper mysqlCharacterHeatMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param role 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public MysqlCharacterHeat selectMysqlCharacterHeatByRole(String role)
    {
        return mysqlCharacterHeatMapper.selectMysqlCharacterHeatByRole(role);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param mysqlCharacterHeat 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MysqlCharacterHeat> selectMysqlCharacterHeatList(MysqlCharacterHeat mysqlCharacterHeat)
    {
        return mysqlCharacterHeatMapper.selectMysqlCharacterHeatList(mysqlCharacterHeat);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param mysqlCharacterHeat 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMysqlCharacterHeat(MysqlCharacterHeat mysqlCharacterHeat)
    {
        return mysqlCharacterHeatMapper.insertMysqlCharacterHeat(mysqlCharacterHeat);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param mysqlCharacterHeat 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMysqlCharacterHeat(MysqlCharacterHeat mysqlCharacterHeat)
    {
        return mysqlCharacterHeatMapper.updateMysqlCharacterHeat(mysqlCharacterHeat);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param roles 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteMysqlCharacterHeatByRoles(String[] roles)
    {
        return mysqlCharacterHeatMapper.deleteMysqlCharacterHeatByRoles(roles);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param role 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteMysqlCharacterHeatByRole(String role)
    {
        return mysqlCharacterHeatMapper.deleteMysqlCharacterHeatByRole(role);
    }
}
