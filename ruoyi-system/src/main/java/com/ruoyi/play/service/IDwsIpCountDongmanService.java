package com.ruoyi.play.service;

import java.util.List;
import com.ruoyi.play.domain.DwsIpCountDongman;

/**
 * 所有的ip地区对应的数量Service接口
 * 
 * @author ruoyi
 * @date 2024-07-12
 */
public interface IDwsIpCountDongmanService 
{
    /**
     * 查询所有的ip地区对应的数量
     * 
     * @param ipLocation 所有的ip地区对应的数量主键
     * @return 所有的ip地区对应的数量
     */
    public DwsIpCountDongman selectDwsIpCountDongmanByIpLocation(String ipLocation);

    /**
     * 查询所有的ip地区对应的数量列表
     * 
     * @param dwsIpCountDongman 所有的ip地区对应的数量
     * @return 所有的ip地区对应的数量集合
     */
    public List<DwsIpCountDongman> selectDwsIpCountDongmanList(DwsIpCountDongman dwsIpCountDongman);

    /**
     * 新增所有的ip地区对应的数量
     * 
     * @param dwsIpCountDongman 所有的ip地区对应的数量
     * @return 结果
     */
    public int insertDwsIpCountDongman(DwsIpCountDongman dwsIpCountDongman);

    /**
     * 修改所有的ip地区对应的数量
     * 
     * @param dwsIpCountDongman 所有的ip地区对应的数量
     * @return 结果
     */
    public int updateDwsIpCountDongman(DwsIpCountDongman dwsIpCountDongman);

    /**
     * 批量删除所有的ip地区对应的数量
     * 
     * @param ipLocations 需要删除的所有的ip地区对应的数量主键集合
     * @return 结果
     */
    public int deleteDwsIpCountDongmanByIpLocations(String[] ipLocations);

    /**
     * 删除所有的ip地区对应的数量信息
     * 
     * @param ipLocation 所有的ip地区对应的数量主键
     * @return 结果
     */
    public int deleteDwsIpCountDongmanByIpLocation(String ipLocation);
}
