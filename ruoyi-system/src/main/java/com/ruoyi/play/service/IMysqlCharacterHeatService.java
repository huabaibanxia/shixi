package com.ruoyi.play.service;

import com.ruoyi.play.domain.MysqlCharacterHeat;

import java.util.List;


/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2024-07-12
 */
public interface IMysqlCharacterHeatService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param role 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public MysqlCharacterHeat selectMysqlCharacterHeatByRole(String role);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param mysqlCharacterHeat 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MysqlCharacterHeat> selectMysqlCharacterHeatList(MysqlCharacterHeat mysqlCharacterHeat);

    /**
     * 新增【请填写功能名称】
     * 
     * @param mysqlCharacterHeat 【请填写功能名称】
     * @return 结果
     */
    public int insertMysqlCharacterHeat(MysqlCharacterHeat mysqlCharacterHeat);

    /**
     * 修改【请填写功能名称】
     * 
     * @param mysqlCharacterHeat 【请填写功能名称】
     * @return 结果
     */
    public int updateMysqlCharacterHeat(MysqlCharacterHeat mysqlCharacterHeat);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param roles 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteMysqlCharacterHeatByRoles(String[] roles);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param role 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteMysqlCharacterHeatByRole(String role);
}
