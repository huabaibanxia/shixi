package com.ruoyi.play.service;


import com.ruoyi.play.domain.DwsBeautifulCount;

import java.util.ArrayList;
import java.util.List;

/**
* @author Administrator
* @description 针对表【dws_beautiful_count】的数据库操作Service
* @createDate 2024-07-12 16:35:10
*/
public interface DwsBeautifulCountService{
    public List<DwsBeautifulCount> selectDwsBeautifulCountList();

}