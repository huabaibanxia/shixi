package com.ruoyi.play.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.play.mapper.DwsIpCountDongmanMapper;
import com.ruoyi.play.domain.DwsIpCountDongman;
import com.ruoyi.play.service.IDwsIpCountDongmanService;

/**
 * 所有的ip地区对应的数量Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-07-12
 */
@Service
public class DwsIpCountDongmanServiceImpl implements IDwsIpCountDongmanService 
{
    @Autowired
    private DwsIpCountDongmanMapper dwsIpCountDongmanMapper;

    /**
     * 查询所有的ip地区对应的数量
     * 
     * @param ipLocation 所有的ip地区对应的数量主键
     * @return 所有的ip地区对应的数量
     */
    @Override
    public DwsIpCountDongman selectDwsIpCountDongmanByIpLocation(String ipLocation)
    {
        return dwsIpCountDongmanMapper.selectDwsIpCountDongmanByIpLocation(ipLocation);
    }

    /**
     * 查询所有的ip地区对应的数量列表
     * 
     * @param dwsIpCountDongman 所有的ip地区对应的数量
     * @return 所有的ip地区对应的数量
     */
    @Override
    public List<DwsIpCountDongman> selectDwsIpCountDongmanList(DwsIpCountDongman dwsIpCountDongman)
    {
        return dwsIpCountDongmanMapper.selectDwsIpCountDongmanList(dwsIpCountDongman);
    }

    /**
     * 新增所有的ip地区对应的数量
     * 
     * @param dwsIpCountDongman 所有的ip地区对应的数量
     * @return 结果
     */
    @Override
    public int insertDwsIpCountDongman(DwsIpCountDongman dwsIpCountDongman)
    {
        return dwsIpCountDongmanMapper.insertDwsIpCountDongman(dwsIpCountDongman);
    }

    /**
     * 修改所有的ip地区对应的数量
     * 
     * @param dwsIpCountDongman 所有的ip地区对应的数量
     * @return 结果
     */
    @Override
    public int updateDwsIpCountDongman(DwsIpCountDongman dwsIpCountDongman)
    {
        return dwsIpCountDongmanMapper.updateDwsIpCountDongman(dwsIpCountDongman);
    }

    /**
     * 批量删除所有的ip地区对应的数量
     * 
     * @param ipLocations 需要删除的所有的ip地区对应的数量主键
     * @return 结果
     */
    @Override
    public int deleteDwsIpCountDongmanByIpLocations(String[] ipLocations)
    {
        return dwsIpCountDongmanMapper.deleteDwsIpCountDongmanByIpLocations(ipLocations);
    }

    /**
     * 删除所有的ip地区对应的数量信息
     * 
     * @param ipLocation 所有的ip地区对应的数量主键
     * @return 结果
     */
    @Override
    public int deleteDwsIpCountDongmanByIpLocation(String ipLocation)
    {
        return dwsIpCountDongmanMapper.deleteDwsIpCountDongmanByIpLocation(ipLocation);
    }
}
