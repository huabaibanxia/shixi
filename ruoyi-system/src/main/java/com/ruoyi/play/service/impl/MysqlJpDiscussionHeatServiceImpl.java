package com.ruoyi.play.service.impl;

import java.util.List;

import com.ruoyi.play.domain.MysqlJpDiscussionHeat;
import com.ruoyi.play.mapper.MysqlJpDiscussionHeatMapper;
import com.ruoyi.play.service.IMysqlJpDiscussionHeatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-07-12
 */
@Service
public class MysqlJpDiscussionHeatServiceImpl implements IMysqlJpDiscussionHeatService
{
    @Autowired
    private MysqlJpDiscussionHeatMapper mysqlJpDiscussionHeatMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param yearMonth 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public MysqlJpDiscussionHeat selectMysqlJpDiscussionHeatByYearMonth(String yearMonth)
    {
        return mysqlJpDiscussionHeatMapper.selectMysqlJpDiscussionHeatByYearMonth(yearMonth);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param mysqlJpDiscussionHeat 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MysqlJpDiscussionHeat> selectMysqlJpDiscussionHeatList(MysqlJpDiscussionHeat mysqlJpDiscussionHeat)
    {
        return mysqlJpDiscussionHeatMapper.selectMysqlJpDiscussionHeatList(mysqlJpDiscussionHeat);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param mysqlJpDiscussionHeat 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMysqlJpDiscussionHeat(MysqlJpDiscussionHeat mysqlJpDiscussionHeat)
    {
        return mysqlJpDiscussionHeatMapper.insertMysqlJpDiscussionHeat(mysqlJpDiscussionHeat);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param mysqlJpDiscussionHeat 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMysqlJpDiscussionHeat(MysqlJpDiscussionHeat mysqlJpDiscussionHeat)
    {
        return mysqlJpDiscussionHeatMapper.updateMysqlJpDiscussionHeat(mysqlJpDiscussionHeat);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param yearMonths 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteMysqlJpDiscussionHeatByYearMonths(String[] yearMonths)
    {
        return mysqlJpDiscussionHeatMapper.deleteMysqlJpDiscussionHeatByYearMonths(yearMonths);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param yearMonth 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteMysqlJpDiscussionHeatByYearMonth(String yearMonth)
    {
        return mysqlJpDiscussionHeatMapper.deleteMysqlJpDiscussionHeatByYearMonth(yearMonth);
    }
}
