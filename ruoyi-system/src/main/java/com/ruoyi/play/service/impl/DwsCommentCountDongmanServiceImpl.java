package com.ruoyi.play.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.play.mapper.DwsCommentCountDongmanMapper;
import com.ruoyi.play.domain.DwsCommentCountDongman;
import com.ruoyi.play.service.IDwsCommentCountDongmanService;

/**
 * 各动漫的的评论数Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-07-11
 */
@Service
public class DwsCommentCountDongmanServiceImpl implements IDwsCommentCountDongmanService 
{
    @Autowired
    private DwsCommentCountDongmanMapper dwsCommentCountDongmanMapper;

    /**
     * 查询各动漫的的评论数
     * 
     * @param dongmanName 各动漫的的评论数主键
     * @return 各动漫的的评论数
     */
    @Override
    public DwsCommentCountDongman selectDwsCommentCountDongmanByDongmanName(String dongmanName)
    {
        return dwsCommentCountDongmanMapper.selectDwsCommentCountDongmanByDongmanName(dongmanName);
    }

    /**
     * 查询各动漫的的评论数列表
     * 
     * @param dwsCommentCountDongman 各动漫的的评论数
     * @return 各动漫的的评论数
     */
    @Override
    public List<DwsCommentCountDongman> selectDwsCommentCountDongmanList(DwsCommentCountDongman dwsCommentCountDongman)
    {
        return dwsCommentCountDongmanMapper.selectDwsCommentCountDongmanList(dwsCommentCountDongman);
    }

    /**
     * 新增各动漫的的评论数
     * 
     * @param dwsCommentCountDongman 各动漫的的评论数
     * @return 结果
     */
    @Override
    public int insertDwsCommentCountDongman(DwsCommentCountDongman dwsCommentCountDongman)
    {
        return dwsCommentCountDongmanMapper.insertDwsCommentCountDongman(dwsCommentCountDongman);
    }

    /**
     * 修改各动漫的的评论数
     * 
     * @param dwsCommentCountDongman 各动漫的的评论数
     * @return 结果
     */
    @Override
    public int updateDwsCommentCountDongman(DwsCommentCountDongman dwsCommentCountDongman)
    {
        return dwsCommentCountDongmanMapper.updateDwsCommentCountDongman(dwsCommentCountDongman);
    }

    /**
     * 批量删除各动漫的的评论数
     * 
     * @param dongmanNames 需要删除的各动漫的的评论数主键
     * @return 结果
     */
    @Override
    public int deleteDwsCommentCountDongmanByDongmanNames(String[] dongmanNames)
    {
        return dwsCommentCountDongmanMapper.deleteDwsCommentCountDongmanByDongmanNames(dongmanNames);
    }

    /**
     * 删除各动漫的的评论数信息
     * 
     * @param dongmanName 各动漫的的评论数主键
     * @return 结果
     */
    @Override
    public int deleteDwsCommentCountDongmanByDongmanName(String dongmanName)
    {
        return dwsCommentCountDongmanMapper.deleteDwsCommentCountDongmanByDongmanName(dongmanName);
    }
}
