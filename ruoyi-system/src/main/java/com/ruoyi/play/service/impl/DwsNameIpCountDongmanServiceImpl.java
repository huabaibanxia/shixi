package com.ruoyi.play.service.impl;

import java.util.List;

import com.ruoyi.play.domain.DwsNameIpCountDongman;
import com.ruoyi.play.mapper.DwsNameIpCountDongmanMapper;
import com.ruoyi.play.service.IDwsNameIpCountDongmanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 对应动漫下的评论的ip地区对应的数量Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-07-12
 */
@Service
public class DwsNameIpCountDongmanServiceImpl implements IDwsNameIpCountDongmanService
{
    @Autowired
    private DwsNameIpCountDongmanMapper dwsNameIpCountDongmanMapper;

    /**
     * 查询对应动漫下的评论的ip地区对应的数量
     * 
     * @param dongmanName 对应动漫下的评论的ip地区对应的数量主键
     * @return 对应动漫下的评论的ip地区对应的数量
     */
    @Override
    public DwsNameIpCountDongman selectDwsNameIpCountDongmanByDongmanName(String dongmanName)
    {
        return dwsNameIpCountDongmanMapper.selectDwsNameIpCountDongmanByDongmanName(dongmanName);
    }

    /**
     * 查询对应动漫下的评论的ip地区对应的数量列表
     * 
     * @param dwsNameIpCountDongman 对应动漫下的评论的ip地区对应的数量
     * @return 对应动漫下的评论的ip地区对应的数量
     */
    @Override
    public List<DwsNameIpCountDongman> selectDwsNameIpCountDongmanList(DwsNameIpCountDongman dwsNameIpCountDongman)
    {
        return dwsNameIpCountDongmanMapper.selectDwsNameIpCountDongmanList(dwsNameIpCountDongman);
    }

    /**
     * 新增对应动漫下的评论的ip地区对应的数量
     * 
     * @param dwsNameIpCountDongman 对应动漫下的评论的ip地区对应的数量
     * @return 结果
     */
    @Override
    public int insertDwsNameIpCountDongman(DwsNameIpCountDongman dwsNameIpCountDongman)
    {
        return dwsNameIpCountDongmanMapper.insertDwsNameIpCountDongman(dwsNameIpCountDongman);
    }

    /**
     * 修改对应动漫下的评论的ip地区对应的数量
     * 
     * @param dwsNameIpCountDongman 对应动漫下的评论的ip地区对应的数量
     * @return 结果
     */
    @Override
    public int updateDwsNameIpCountDongman(DwsNameIpCountDongman dwsNameIpCountDongman)
    {
        return dwsNameIpCountDongmanMapper.updateDwsNameIpCountDongman(dwsNameIpCountDongman);
    }

    /**
     * 批量删除对应动漫下的评论的ip地区对应的数量
     * 
     * @param dongmanNames 需要删除的对应动漫下的评论的ip地区对应的数量主键
     * @return 结果
     */
    @Override
    public int deleteDwsNameIpCountDongmanByDongmanNames(String[] dongmanNames)
    {
        return dwsNameIpCountDongmanMapper.deleteDwsNameIpCountDongmanByDongmanNames(dongmanNames);
    }

    /**
     * 删除对应动漫下的评论的ip地区对应的数量信息
     * 
     * @param dongmanName 对应动漫下的评论的ip地区对应的数量主键
     * @return 结果
     */
    @Override
    public int deleteDwsNameIpCountDongmanByDongmanName(String dongmanName)
    {
        return dwsNameIpCountDongmanMapper.deleteDwsNameIpCountDongmanByDongmanName(dongmanName);
    }
}
