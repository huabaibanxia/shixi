package com.ruoyi.play.service;

import com.ruoyi.play.domain.MysqlJpDiscussionHeat;

import java.util.List;


/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2024-07-12
 */
public interface IMysqlJpDiscussionHeatService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param yearMonth 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public MysqlJpDiscussionHeat selectMysqlJpDiscussionHeatByYearMonth(String yearMonth);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param mysqlJpDiscussionHeat 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MysqlJpDiscussionHeat> selectMysqlJpDiscussionHeatList(MysqlJpDiscussionHeat mysqlJpDiscussionHeat);

    /**
     * 新增【请填写功能名称】
     * 
     * @param mysqlJpDiscussionHeat 【请填写功能名称】
     * @return 结果
     */
    public int insertMysqlJpDiscussionHeat(MysqlJpDiscussionHeat mysqlJpDiscussionHeat);

    /**
     * 修改【请填写功能名称】
     * 
     * @param mysqlJpDiscussionHeat 【请填写功能名称】
     * @return 结果
     */
    public int updateMysqlJpDiscussionHeat(MysqlJpDiscussionHeat mysqlJpDiscussionHeat);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param yearMonths 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteMysqlJpDiscussionHeatByYearMonths(String[] yearMonths);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param yearMonth 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteMysqlJpDiscussionHeatByYearMonth(String yearMonth);
}
