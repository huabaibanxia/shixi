package com.ruoyi.play.mapper;


import com.ruoyi.play.domain.DwsBeautifulCount;

import java.util.ArrayList;
import java.util.List;

/**
* @author Administrator
* @description 针对表【dws_beautiful_count】的数据库操作Mapper
* @createDate 2024-07-12 16:35:10
* @Entity com.ruoyi.play.domain.DwsBeautifulCount
*/
public interface DwsBeautifulCountMapper {
    public List<DwsBeautifulCount> selectDwsBeautifulCountList();

}