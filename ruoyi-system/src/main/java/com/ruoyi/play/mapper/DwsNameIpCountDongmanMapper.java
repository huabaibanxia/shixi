package com.ruoyi.play.mapper;


import com.ruoyi.play.domain.DwsNameIpCountDongman;

import java.util.List;

/**
 * 对应动漫下的评论的ip地区对应的数量Mapper接口
 *
 * @author ruoyi
 * @date 2024-07-12
 */
public interface DwsNameIpCountDongmanMapper {
    /**
     * 查询对应动漫下的评论的ip地区对应的数量
     *
     * @param dongmanName 对应动漫下的评论的ip地区对应的数量主键
     * @return 对应动漫下的评论的ip地区对应的数量
     */
    public DwsNameIpCountDongman selectDwsNameIpCountDongmanByDongmanName(String dongmanName);

    /**
     * 查询对应动漫下的评论的ip地区对应的数量列表
     *
     * @param dwsNameIpCountDongman 对应动漫下的评论的ip地区对应的数量
     * @return 对应动漫下的评论的ip地区对应的数量集合
     */
    public List<DwsNameIpCountDongman> selectDwsNameIpCountDongmanList(DwsNameIpCountDongman dwsNameIpCountDongman);

    /**
     * 新增对应动漫下的评论的ip地区对应的数量
     *
     * @param dwsNameIpCountDongman 对应动漫下的评论的ip地区对应的数量
     * @return 结果
     */
    public int insertDwsNameIpCountDongman(DwsNameIpCountDongman dwsNameIpCountDongman);

    /**
     * 修改对应动漫下的评论的ip地区对应的数量
     *
     * @param dwsNameIpCountDongman 对应动漫下的评论的ip地区对应的数量
     * @return 结果
     */
    public int updateDwsNameIpCountDongman(DwsNameIpCountDongman dwsNameIpCountDongman);

    /**
     * 删除对应动漫下的评论的ip地区对应的数量
     *
     * @param dongmanName 对应动漫下的评论的ip地区对应的数量主键
     * @return 结果
     */
    public int deleteDwsNameIpCountDongmanByDongmanName(String dongmanName);

    /**
     * 批量删除对应动漫下的评论的ip地区对应的数量
     *
     * @param dongmanNames 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDwsNameIpCountDongmanByDongmanNames(String[] dongmanNames);
}
