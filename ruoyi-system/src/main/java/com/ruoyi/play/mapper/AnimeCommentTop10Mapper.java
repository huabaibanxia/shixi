package com.ruoyi.play.mapper;

import com.ruoyi.play.domain.AnimeCommentTop10;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2024-07-12
 */
public interface AnimeCommentTop10Mapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param animeName 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public AnimeCommentTop10 selectAnimeCommentTop10ByAnimeName(String animeName);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param animeCommentTop10 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<AnimeCommentTop10> selectAnimeCommentTop10List(AnimeCommentTop10 animeCommentTop10);

    /**
     * 新增【请填写功能名称】
     * 
     * @param animeCommentTop10 【请填写功能名称】
     * @return 结果
     */
    public int insertAnimeCommentTop10(AnimeCommentTop10 animeCommentTop10);

    /**
     * 修改【请填写功能名称】
     * 
     * @param animeCommentTop10 【请填写功能名称】
     * @return 结果
     */
    public int updateAnimeCommentTop10(AnimeCommentTop10 animeCommentTop10);

    /**
     * 删除【请填写功能名称】
     * 
     * @param animeName 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteAnimeCommentTop10ByAnimeName(String animeName);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param animeNames 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAnimeCommentTop10ByAnimeNames(String[] animeNames);
}
