package com.ruoyi.play.mapper;

import java.util.List;
import com.ruoyi.play.domain.DwsCommentCountDongman;

/**
 * 各动漫的的评论数Mapper接口
 * 
 * @author ruoyi
 * @date 2024-07-11
 */
public interface DwsCommentCountDongmanMapper 
{
    /**
     * 查询各动漫的的评论数
     * 
     * @param dongmanName 各动漫的的评论数主键
     * @return 各动漫的的评论数
     */
    public DwsCommentCountDongman selectDwsCommentCountDongmanByDongmanName(String dongmanName);

    /**
     * 查询各动漫的的评论数列表
     * 
     * @param dwsCommentCountDongman 各动漫的的评论数
     * @return 各动漫的的评论数集合
     */
    public List<DwsCommentCountDongman> selectDwsCommentCountDongmanList(DwsCommentCountDongman dwsCommentCountDongman);

    /**
     * 新增各动漫的的评论数
     * 
     * @param dwsCommentCountDongman 各动漫的的评论数
     * @return 结果
     */
    public int insertDwsCommentCountDongman(DwsCommentCountDongman dwsCommentCountDongman);

    /**
     * 修改各动漫的的评论数
     * 
     * @param dwsCommentCountDongman 各动漫的的评论数
     * @return 结果
     */
    public int updateDwsCommentCountDongman(DwsCommentCountDongman dwsCommentCountDongman);

    /**
     * 删除各动漫的的评论数
     * 
     * @param dongmanName 各动漫的的评论数主键
     * @return 结果
     */
    public int deleteDwsCommentCountDongmanByDongmanName(String dongmanName);

    /**
     * 批量删除各动漫的的评论数
     * 
     * @param dongmanNames 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDwsCommentCountDongmanByDongmanNames(String[] dongmanNames);
}
