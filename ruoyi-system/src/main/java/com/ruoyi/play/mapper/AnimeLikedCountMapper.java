package com.ruoyi.play.mapper;


import com.ruoyi.play.domain.AnimeLikedCount;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2024-07-12
 */
public interface AnimeLikedCountMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param animeName 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public AnimeLikedCount selectAnimeLikedCountByAnimeName(String animeName);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param animeLikedCount 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<AnimeLikedCount> selectAnimeLikedCountList(AnimeLikedCount animeLikedCount);

    /**
     * 新增【请填写功能名称】
     * 
     * @param animeLikedCount 【请填写功能名称】
     * @return 结果
     */
    public int insertAnimeLikedCount(AnimeLikedCount animeLikedCount);

    /**
     * 修改【请填写功能名称】
     * 
     * @param animeLikedCount 【请填写功能名称】
     * @return 结果
     */
    public int updateAnimeLikedCount(AnimeLikedCount animeLikedCount);

    /**
     * 删除【请填写功能名称】
     * 
     * @param animeName 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteAnimeLikedCountByAnimeName(String animeName);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param animeNames 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAnimeLikedCountByAnimeNames(String[] animeNames);
}
