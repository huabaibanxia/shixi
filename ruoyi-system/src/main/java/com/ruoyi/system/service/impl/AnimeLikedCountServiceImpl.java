package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.play.domain.AnimeLikedCount;
import com.ruoyi.play.mapper.AnimeLikedCountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.system.service.IAnimeLikedCountService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-07-12
 */
@Service
public class AnimeLikedCountServiceImpl implements IAnimeLikedCountService 
{
    @Autowired
    private AnimeLikedCountMapper animeLikedCountMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param animeName 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public AnimeLikedCount selectAnimeLikedCountByAnimeName(String animeName)
    {
        return animeLikedCountMapper.selectAnimeLikedCountByAnimeName(animeName);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param animeLikedCount 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<AnimeLikedCount> selectAnimeLikedCountList(AnimeLikedCount animeLikedCount)
    {
        return animeLikedCountMapper.selectAnimeLikedCountList(animeLikedCount);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param animeLikedCount 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertAnimeLikedCount(AnimeLikedCount animeLikedCount)
    {
        return animeLikedCountMapper.insertAnimeLikedCount(animeLikedCount);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param animeLikedCount 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateAnimeLikedCount(AnimeLikedCount animeLikedCount)
    {
        return animeLikedCountMapper.updateAnimeLikedCount(animeLikedCount);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param animeNames 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteAnimeLikedCountByAnimeNames(String[] animeNames)
    {
        return animeLikedCountMapper.deleteAnimeLikedCountByAnimeNames(animeNames);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param animeName 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteAnimeLikedCountByAnimeName(String animeName)
    {
        return animeLikedCountMapper.deleteAnimeLikedCountByAnimeName(animeName);
    }
}
