package com.ruoyi.system.service;

import com.ruoyi.play.domain.AnimeCommentTop10;

import java.util.List;


/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2024-07-12
 */
public interface IAnimeCommentTop10Service 
{


    /**
     * 查询【请填写功能名称】列表
     * 
     * @param animeCommentTop10 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<AnimeCommentTop10> selectAnimeCommentTop10List(AnimeCommentTop10 animeCommentTop10);

}
