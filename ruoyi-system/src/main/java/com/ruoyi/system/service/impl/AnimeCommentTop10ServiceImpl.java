package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.play.domain.AnimeCommentTop10;
import com.ruoyi.play.mapper.AnimeCommentTop10Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.system.service.IAnimeCommentTop10Service;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2024-07-12
 */
@Service
public class AnimeCommentTop10ServiceImpl implements IAnimeCommentTop10Service {
    @Autowired
    private AnimeCommentTop10Mapper animeCommentTop10Mapper;

    @Override
    public List<AnimeCommentTop10> selectAnimeCommentTop10List(AnimeCommentTop10 animeCommentTop10) {
        return animeCommentTop10Mapper.selectAnimeCommentTop10List(animeCommentTop10);
    }
}
